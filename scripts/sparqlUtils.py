import pandas as pd
import requests
from pandas.io.json import json_normalize
import json

def loadPDfromSPARQL (indata, isFile = True, columns = False):
    
    '''
        A function that reads a json produced by a SPARQL
        query and converts it to a pandas dataframe.
        
        Mandatory arguments
        
        i. indata: if isFile = True, a filepath to the json.
        otherwise, the json object itself
        
        KeyWord arguments
        
        i. isFile: specify if a file should be read, or a json
        '''
    
    ## if indata is a file, open it
    if isFile:
        infile = indata
        jsonfile = open(infile)
        result = json.load (jsonfile)
        jsonfile.close()
    ## otherwise read the json object
    else:
        result = json.loads (indata)
    ## get the json 'bindings' that contain the necessary data
    if not  (len (result["results"]["bindings"])):
        return pd.DataFrame(columns=result["head"]["vars"])
    else:
        table = json_normalize(result["results"]["bindings"])
        ## get the value properties of the columns (there's also a 'type' property)
        ## if the column names are passed through the 'columns' keyword, get it there
        
        
        if columns:
            relevant_columns = ['%s.value' %c for c in  columns if '%s.value' %c in table.columns]
        else:
            relevant_columns = [c for c in table.columns if '.value' in c and c in table.columns]
        ## and just retain them
        table = table[relevant_columns]
        
        ## change the column names to remove the '.value' suffix
        table.columns = [c.replace ('.value', '') for c in table.columns]
        
        ## get the original column order
        cOrder = [c for c in result["head"]["vars"] if c in table.columns.values]
        ## and return the ordered table
        return table [cOrder]




def sendQuery (Q, endpoint, columns = False, **kwargs):
    '''
        A function that gets a SPARQL formatted query,
        posts it to the endpoint specified and returns a
        pandas dataframe with the response retrieved from
        the endpoint.
        
        Mandatory arguments:
        
        i. Q: the query. Must be a valid sparql query
        
        KeyWord arguments:
        
        i. endpoint : the web address of the sparql endpoint
        ii. columns : the name of the columns, i.e. the variables queried
        '''
    
    ## put the query in a dict
    query = {'query': Q, 'format' :'json'}
    ## and use requests to post the query
    r = requests.post(endpoint, data=query)
    
    ## return a pandas dataframe with the response
    return loadPDfromSPARQL (r.text, isFile=False, columns = columns)
