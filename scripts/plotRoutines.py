import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from matplotlib.ticker import FuncFormatter
import pandas as pd

def euroTick(x, pos):
    'The two args are the value and tick position'
    return '%gM€' %(x*1e-6)


def percentTick(x, pos):
    'The two args are the value and tick position'
    return '%d%%' %(x*1e2)

def percentTick2(x, pos):
    'The two args are the value and tick position'
    return ('%g%%' %(x*1e2)) #.replace('.',',')


def yearTick(x, pos):
    'The two args are the value and tick position'
    return '%d' %(x)

def commaTick(x, pos):
    'The two args are the value and tick position'
    return ('%g' %(x)).replace('.',',')

formatterPerc = FuncFormatter(percentTick)
formatterPerc2 = FuncFormatter(percentTick2)
formatterYear = FuncFormatter(yearTick)
formatterComma = FuncFormatter(commaTick)
formatterEuro = FuncFormatter(euroTick)

# sdg_sheetID = '11myjrE3ZtyS4t9mBoYiZnWW9BVU96wnETybYXa1q7gs'
# sdg_colors_names_sheet = 'Sheet1'
# sdg_names_colors = g2d.download(sdg_sheetID, sdg_colors_names_sheet,\
#                                 col_names=True, row_names=True)

def circularplot (geoPerimeter,\
                  percent, \
                  total,\
                  baseline,\
                  plot_colors,\
                 plot_order,
                 rmax,\
                 filename):
    
    
    '''
    Make a circular (flower) plot, given
    geoperimeter (str)- the (column) name of the data to plot (e.g. Italy)
    percent (pandas dataframe)- the dataframe with % of records
        it has to feature a column matching `geoperimeter`
    total (pandas dataframe)- the dataframe with total number of records
        it has to feature a column matching `geoperimeter`
    baseline (pandas Series)- the series with reference % of records
        the index must be the same of the previous two dataframes
    plot_colors (pandas Series)- the series with reference colors
        the index must be the same of the above data
    plot_order (iterable)- a sorted subset of the index of the above data
    rmax (float)- the upper limit of the radial axis
    filename (str)- the name of the output file to generate
    '''
    # Compute pie slices
    N = percent[geoPerimeter].shape[0]
    theta = np.linspace(0.0, 2 * np.pi, N, endpoint=False)

    ## the radii are the percentages
    radii = percent[geoPerimeter].reindex(plot_order).fillna(0)
    mean_radius = radii.mean()
    width = (2*np.pi)/N *0.9
    
    ## compute specialisation indices
    si_values = radii / baseline.reindex(plot_order).fillna(0)


    ## keep track of the number of records
    npubs = total[geoPerimeter].reindex(plot_order).fillna(0).astype(int)
    
    
    ## the colors are given, to be sorted
    ## in this specific order
    colors = plot_colors.loc[plot_order]
    labels = plot_order

    
    ## do the plot
    ## specifying a bottom value
    BOTTOM = 0.1
    ax = plt.subplot(111, projection='polar')
    bars = ax.bar(theta, radii, width=width, bottom=BOTTOM, color = colors, zorder = 1)
    
    ## make a box around values with a SI > 1
    ax.bar(theta[si_values > 1], radii[si_values > 1], width=width, \
           bottom=BOTTOM, color = 'none', edgecolor = '#484848', zorder = 1, lw = 1.5)

    
    ## put labels

    ## compute rotations of the labels
    rotations = np.rad2deg(theta)

    ## go around the circle and put labels
    for x, bar, rotation, label, npbs, perc, si in \
            zip(theta, bars, rotations, plot_order, npubs, 
                                                       radii, si_values):
        
        ## normally, labels outside the bars are
        ## left aligned and those inside are right aligned
        ha_in = 'right'
        ha_out = 'left'
        
        ## but reverse the alignment
        ## when the rotation is 180
        if rotation == 180:
            rotation = 0
            ha_in = 'left'
            ha_out = 'right'

        ## specify radial position and text of labels
        r_pos = BOTTOM+bar.get_height() + .01
            
        lab_text = '''%d
%g%%''' %(npbs,\
                   round(perc*100,2),\
                   )
        fweight = 'medium'
        fsize = 12
        
        ## if specialisation > 1
        ## make the label boldface and with bigger
        ## fonts
        if si > 1.:
            fweight = 'bold'
            fsize = 13
            
        ## inside label, for SI
        lab_si = ax.text(x, BOTTOM-0.01, round(si,2), 
                 ha=ha_in, va='center', rotation=rotation, rotation_mode="anchor",\
                      fontweight = fweight, fontsize = fsize) 
        
        
        ## outside label, for N. & % of records
        lab_sdg = ax.text(x, r_pos, lab_text, 
                 ha=ha_out, va='center', rotation=rotation, rotation_mode="anchor",\
                      fontweight = 'medium', fontsize = 12)         


    ax.axis('off')
    ax.set_title ('''%s'''%geoPerimeter,\
                   pad = -5, fontweight = 'bold', fontsize = 25)
    
    ## shared max value of the radial axis
    
    ax.set_ylim([0, rmax])
    fig = plt.gcf()
    fig.set_size_inches(7,7)
    fig.tight_layout()
    

    fig.savefig('../data/processed/%s.png' % filename,\
                bbox_inches='tight', dpi = 200)
    
    plt.close()


def multiLineText (txt, span = 3):
    '''Split a long text into a 
    multiline string.
    Arguments:
        txt (str)- the input text
        [optional] span (int)- the length of each line
            of the returned text
        
    Returns:
        out (str)- a multiline verions of txt
    '''
    out = txt.split()
    out = [' '.join(out[idx:idx+span]) for idx in range(0,len(out),span)]
    
    return '\n'.join(out)
    
def circularplotLegend (\
                  plot_colors,\
                 plot_order,
                 filename):
    
    
    '''
    Make the legend of a circular (flower) plot, given
    plot_colors (pandas Series)- the series with reference % of records
        the index must be the same of the above data
    plot_order (iterable)- a sorted subset of the index of the above data
    filename (str)- the name of the output file to generate
    '''
    # Compute pie slices
    N = len ( plot_order )
    theta = np.linspace(0.0, 2 * np.pi, N, endpoint=False)

    ## the radii are the percentages
    radii = np.array([1. for _ in range(N)])
    mean_radius = radii.mean()
    width = (2*np.pi)/N *0.9
    

    ## the colors are given, to be sorted
    ## in this specific order
    colors = plot_colors.loc[plot_order]
    labels = plot_order

    
    ## do the plot
    ## specifying a bottom value
    BOTTOM = 0.5
    ax = plt.subplot(111, projection='polar')
    bars = ax.bar(theta, radii, width=width, bottom=BOTTOM, color = colors, zorder = 1)
    
    
    
    ## put labels

    ## compute rotations of the labels
    rotations = np.rad2deg(theta)

    ## go around the circle and put labels
    for x, bar, rotation, label, perc, in \
            zip(theta, bars, rotations, plot_order, 
                                                       radii):
        
        ## normally, labels outside the bars are
        ## left aligned and those inside are right aligned
        ha_in = 'right'
        ha_out = 'left'
        
        ## but reverse the alignment
        ## when the rotation is 180
        if rotation == 180:
            rotation = 0
            ha_in = 'left'
            ha_out = 'right'

        ## specify radial position and text of labels
        r_pos = BOTTOM+bar.get_height() + .01
            
        lab_text = '''N. registri
% registri''' 
        fweight = 'medium'
        fsize = 12
                    
        
        ## outside label, for N. & % of records
        if x == theta[0]:
            lab_si = ax.text(x, BOTTOM-0.01, 'Specialisation\nIndex', 
                 ha=ha_in, va='center', rotation=rotation, rotation_mode="anchor",\
                      fontweight = fweight, fontsize = fsize) 
        
        
            lab_sdg = ax.text(x, r_pos, lab_text, 
                     ha=ha_out, va='center', rotation=rotation, rotation_mode="anchor",\
                          fontweight = 'medium', fontsize = 12)         
        
#         label_legend = [' '.join(label.split())
        label_legend = ax.text(x, BOTTOM+0.03, multiLineText(label), 
                     ha=ha_out, va='center', rotation=rotation, rotation_mode="anchor",\
                          fontweight = 'bold', fontsize = 12, color = 'white')         


    ax.axis('off')
    ax.set_title ('''Legenda''',\
                   pad = -5, fontweight = 'bold', fontsize = 25)
    
    ## shared max value of the radial axis
    
    ax.set_ylim([0, 1.5])
    fig = plt.gcf()
    fig.set_size_inches(7,7)
    fig.tight_layout()
    

    fig.savefig('../data/processed/%s.png' % filename,\
                bbox_inches='tight', dpi = 200)
    
    plt.close()